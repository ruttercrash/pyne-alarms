#!/usr/bin/env python
import csv
import datetime
import re

#from playsound import playsound

WEEKDAYS = 'Mo Di Mi Do Fr Sa So'.split()


class MyDateTime:
    def __init__(self):
        self.month, self.day, self.weekday, self.hour, self.minute = None, None, None, None, None

    def __eq__(self, other):
        if self.hour == other.hour and self.minute == other.minute:
            if self.month:
                return (self.month == other.month and
                        self.day == other.day)
            if self.weekday:
                return self.weekday == other.weekday


class Now(MyDateTime):
    def __init__(self):
        super().__init__()

        now = datetime.datetime.now()
        self.month = now.month
        self.day = now.day
        self.weekday = WEEKDAYS[now.weekday()]
        self.hour = now.hour
        self.minute = now.minute


class Alarm(MyDateTime):
    def __init__(self, time: str, date_or_weekday: str = None):
        super().__init__()

        self.date_or_weekday = date_or_weekday
        self.weekday = date_or_weekday if date_or_weekday in WEEKDAYS else None
        self.month, self.day = self.from_string(date_or_weekday)
        self.hour, self.minute = self.from_string(time)

    def from_string(self, time_or_date: str) -> datetime:
        time_pattern = r'^\d{1,2}\:\d{1,2}$'
        date_pattern = r'^\d{1,2}\.\d{1,2}\.'

        if re.search(time_pattern, time_or_date):
            time = datetime.datetime.strptime(time_or_date, '%H:%M')
            if not self.date_or_weekday:
                now = Now()
                self.month, self.day = now.month, now.day
            return time.hour, time.minute

        if re.search(date_pattern, time_or_date):
            date = datetime.datetime.strptime(time_or_date, '%m:%d')
            return date.month, date.day

    def is_now(self):
        return self == Now()


class Alarms:
    def __init__(self, settings_file):
        user_settings = [line for line in
                         csv.reader(open(settings_file))]
        self.alarms = Alarms.set_alarms(user_settings)

    @staticmethod
    def set_alarms(user_settings):
        alarms = []
        for user_setting in user_settings:
            time, *dates_or_weekdays = user_setting
            if dates_or_weekdays:
                for date_or_weekday in dates_or_weekdays:
                    alarms.append(Alarm(time, date_or_weekday))
            else:
                alarms.append(Alarm(time))
        return alarms

    def __iter__(self):
        return iter(self.alarms)


def main():
    alarms = Alarms('alarm_settings.csv')
    for alarm in alarms:
        if alarm.is_now:
            #playsound('alarm.mp3')
            pass


if __name__ == '__main__':
    pass
