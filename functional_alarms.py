#!/usr/bin/env python

""" Plays alarm sound when user defined time and optional date or weekday is reached.

Reads alarm settings from csv file e. g.:
9:30
10:00,Mo,Mi,Fr
00:00,31.12.,1.1.
"""

import csv
from datetime import datetime
import itertools

# from playsound import playsound
from pprint import pprint

WEEKDAYS = 'Mo Di Mi Do Fr Sa So'.split()
ALARM_KEYS = 'hour minute month day weekday'.split()
NONE = itertools.cycle([None])
ALARM = {k: v for (k, v) in zip(ALARM_KEYS, NONE)}


def parse_config(settings_csv):
    with open(settings_csv) as settings_file:
        settings = list(csv.reader(settings_file))
    return settings


def make_time_dates_split(settings):
    return [
        [alarm[begin:end]
         for begin, end, in ((0, 1), (1, None))]
        for alarm in settings
    ]


def make_hours_minutes_split(time_dates_split):
    return [
        time.split(':')
        for alarm in time_dates_split
        for time in alarm[0]
    ]


def make_time_dicts(time):
    return {'hour': int(time[0]), 'minute': int(time[1])}


def make_weekdays_dict_dates_split(time_dates_split):
    return [
        [{'weekday': WEEKDAYS.index(date_or_weekday)}
         if date_or_weekday in WEEKDAYS else
         date_or_weekday.split('.')[:-1]
         if date_or_weekday is not None and date_or_weekday not in WEEKDAYS else
         time_and_dates_or_weekdays
         for date_or_weekday in time_and_dates_or_weekdays[1]]
        for time_and_dates_or_weekdays in time_dates_split
    ]


def make_dates_dicts(date_or_weekday):
    return (
        {'day': date_or_weekday[0], 'month': date_or_weekday[1]}
        if isinstance(date_or_weekday, list) else date_or_weekday
    )


def make_now_dates_for_empties(dates_or_weekdays):
    return (
        [{'month': datetime.now().month, 'day': datetime.now().day}]
        if not dates_or_weekdays else dates_or_weekdays
    )

    # TODO: alarms = ???



def is_now(alarm: list[str]):
    now = datetime.datetime.now()
    user_time, *user_dates_or_weekdays = alarm
    alarm_time = datetime.datetime.strptime(user_time, '%H:%M')

    if (alarm_time.hour == now.hour and
            alarm_time.minute == now.minute):

        if not user_dates_or_weekdays:
            return True

        for date_or_weekday in user_dates_or_weekdays:
            if date_or_weekday in WEEKDAYS:
                return WEEKDAYS[now.weekday()] == date_or_weekday

            alarm_date = datetime.datetime.strptime(date_or_weekday, '%m.%d.')
            return alarm_date.month == now.month and alarm_date.day == now.day


def main():
    alarms_settings = parse_config('alarms.csv')
    for alarm in alarms_settings:
        if is_now(alarm):
            #playsound('alarm.mp3')
            pass


if __name__ == '__main__':
    settings = parse_config('alarms.csv')
    time_dates_split = make_time_dates_split(settings)
    hours_minutes_split = make_hours_minutes_split(time_dates_split)
    time_dicts = list(map(make_time_dicts, hours_minutes_split))
    weekdays_dict_dates_split = make_weekdays_dict_dates_split(time_dates_split)
    dates_dicts = map(make_dates_dicts, weekdays_dict_dates_split)
    now_dates_for_empties = map(make_now_dates_for_empties, dates_dicts)
